#include <iostream>
#include <string>
#include "mpi.h"

const int size1 = 3;
const int size2 = 4;
const int master = 0;
const int from_master = 1;
const int from_worker = 2;

using namespace std;
int main(int argc, char* argv[]) {
    double array1[size1][size2] = {
        {1,2,3,4},
        {5,6,7,8},
        {9,10,11,12}
    };
    double array2[size2][size1] = {
        {1,2,3},
        {4,5,6},
        {7,8,9},
        {10,11,12}
    };
    double arrayResult[size1][size1];
    //  70   80   90
    // 158  184  210
    // 246  288  330

    int rc = 0; //хз что это, но Abort не работает без rc
    
    int rank;
    int size;
    
    MPI_Status status;
    
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    if (size != 4) {
        printf("Необходимо 4 процесса. Выход...\n");
        MPI_Abort(MPI_COMM_WORLD, rc);
        exit(1);
    };
    
    
    //************* основная задача *************
    if (rank == master){
        // отпрвка
        for (int i=1; i<size; i++) {
            int currentWorker=i;
            MPI_Send(&array1[i-1], size2, MPI_DOUBLE, currentWorker, from_master, MPI_COMM_WORLD);
            MPI_Send(&array2, size1*size2, MPI_DOUBLE, currentWorker, from_master, MPI_COMM_WORLD);
        }
        
        // получение
        for (int i=1; i<size; i++){
            int currentWorker=i;
            MPI_Recv(&arrayResult[currentWorker-1], size1, MPI_DOUBLE, currentWorker, from_worker, MPI_COMM_WORLD, &status);
        }
        
        // вывод результата
        for (int i=0; i<size1; i++) {
            for (int j=0; j<size1; j++) {
                cout << arrayResult[i][j] << ' ';
            }
            cout << endl;
        }
    }
    
    
    //************* работники *************
    if (rank>master){
        double array1_temp[size2];
        double array2_temp[size2][size1];
        MPI_Recv(&array1_temp, size2, MPI_DOUBLE,/*source*/ master,/*type*/ from_master, MPI_COMM_WORLD, &status);
        MPI_Recv(&array2_temp, size1*size2, MPI_DOUBLE,/*source*/ master,/*type*/ from_master, MPI_COMM_WORLD, &status);
        
        double arrayResult_temp[size1];

        for (int i=0; i<size2; i++) {
            for (int j=0; j<size2; j++) {
                arrayResult_temp[i] += array1_temp[j] * array2_temp[j][i];
            }
        }
        
        MPI_Send(&arrayResult_temp, size1, MPI_DOUBLE, master, from_worker, MPI_COMM_WORLD);
    };

    MPI_Finalize();
    return 0;
}
